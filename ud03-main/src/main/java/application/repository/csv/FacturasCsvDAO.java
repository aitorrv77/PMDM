package application.repository.csv;

import java.io.BufferedWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import application.model.Cliente;
import application.model.Factura;
import application.model.LineasFactura;
import application.model.Vendedores;


@Repository
public class FacturasCsvDAO {
	
	public File writeCSV(Factura factura) {
		float totalImporte = 0;
		Vendedores vendedor = new Vendedores();
		Cliente cliente =  new Cliente();
		File file;
		
		try {
			file = File.createTempFile("facturas", ".csv");
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		try(
				OutputStream ou = new FileOutputStream(file);
				OutputStreamWriter osw = new OutputStreamWriter(ou, "UTF-8");
				BufferedWriter bf = new BufferedWriter(osw);
		)
		{
			
			bf.write("----------------------------------------------");
			bf.newLine();
			bf.write("-Factura: "+factura.getId()+" ");
			bf.write("Fecha: "+factura.getFecha()+". -");
			bf.newLine();
			bf.write("-Articulos"+"			");
			bf.write("CANTIDAD"+"			");
			bf.write("IMPORTE. -");
			bf.newLine();
			bf.write("----------------------------------------------");
			bf.newLine();
			for(LineasFactura lfactura:factura.getlFactura()) {
				
				bf.write("Articulo: ");
				bf.write(lfactura.getArticulo().getId());
				bf.write(lfactura.getArticulo().getNombre());
				bf.write(lfactura.getCantidad());
				bf.write((int) lfactura.getImporte());
				totalImporte = lfactura.getImporte() + lfactura.getImporte();
			}
			bf.newLine();
			bf.write("----------------------------------------------");
			bf.newLine();
			bf.write("TOTAL");
			bf.write((int) totalImporte);
			bf.newLine();
			bf.newLine();
			bf.write("Cliente: "+ factura.getCliente().getNombre()+"		"+"Vendedor: "+factura.getVendedor().getNombre()+"\n"+"		"+factura.getCliente().getDireccion()+" "	);
			bf.newLine();
			bf.write("Forma de pago: "+factura.getFormaPago());
			return file;
			
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
	}

	

	
}
