package application.repository.jdbc.facturas;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import application.model.Factura;

public class FacturasRowMapper implements RowMapper<Factura>{

	@Override
	public Factura mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Factura factura = new Factura();
		factura.setId(rs.getInt("id"));
		factura.setFecha(rs.getDate("fecha"));
		factura.setIdcliente(rs.getInt("cliente"));
		factura.setIdvendedor(rs.getInt("vendedor"));
		factura.setFormaPago(rs.getString("formapago"));
		return factura;
	}

}
