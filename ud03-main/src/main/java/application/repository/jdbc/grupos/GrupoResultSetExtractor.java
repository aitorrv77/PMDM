package application.repository.jdbc.grupos;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import application.model.Grupo;

public class GrupoResultSetExtractor implements ResultSetExtractor<Grupo>{

	@Override
	public Grupo extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(!rs.next()) {
		return null;
		}
		
		Grupo grupo = new Grupo();
		
		grupo.setId(rs.getInt("id"));
		grupo.setDescripcion(rs.getString("descripcion"));
		
		return grupo;
	}

}
