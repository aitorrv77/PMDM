package application.model;

public enum TipoFiltro {
	STRING,
	INTEGER,
	FLOAT,
	DATE
}
