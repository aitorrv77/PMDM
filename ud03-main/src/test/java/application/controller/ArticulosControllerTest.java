package application.controller;

import org.junit.jupiter.api.TestMethodOrder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import application.model.Articulo;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
public class ArticulosControllerTest {
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	private String getUrlBase() {
		return "http://localhost:"+this.port+"/api-test/articulos";
		
	}
	
	private Integer getCount() {
		String URL = this.getUrlBase()+"/count";
		
		HttpHeaders headers = new HttpHeaders();
		
		headers.set("Accept", "application/json");
		
		HttpEntity httpEntity = new HttpEntity(headers);
		
		ResponseEntity<Integer> respuesta = restTemplate.exchange(URL,HttpMethod.GET,httpEntity,Integer.class);
		
		assertEquals(HttpStatus.OK,respuesta.getStatusCode());
		
		return respuesta.getBody();
	}
	
	@Test
	@Order(1)
	void testCount() {
		assertEquals(8, this.getCount());
	}
	
	@Test
	@Order(2)
	void testCreate() {
		assertEquals(8, this.getCount());
		
		Articulo articulo = new Articulo();
		articulo.setNombre("teclado");
		articulo.setPrecio((double) 32);
		articulo.setCodigo("tec3");
		articulo.setGrupo(2);
		
		String service = this.getUrlBase();
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", "application/json");
		
		HttpEntity<Articulo> entity = new HttpEntity<>(articulo,headers);
		
		ResponseEntity<Articulo> respuesta = this.restTemplate.exchange(
														service,
														HttpMethod.POST,
														entity,
														Articulo.class);
		
		assertEquals(HttpStatus.CREATED, respuesta.getStatusCode());
		
		assertNotNull(respuesta.getBody().getId());
		assertEquals(articulo.getNombre(), respuesta.getBody().getNombre());
		assertEquals(articulo.getPrecio(), respuesta.getBody().getPrecio());
		assertEquals(articulo.getCodigo(), respuesta.getBody().getCodigo());
		assertEquals(articulo.getGrupo(), respuesta.getBody().getGrupo());
		
		assertEquals(9, this.getCount());
		
	}
	
	@Test
	@Order(3)
	void testFindById() {
		String service = this.getUrlBase()+"/{id}";
		
		Map<String, String> params = new HashMap<>();
		params.put("id", "1");
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", "application/json");
		
		HttpEntity entity = new HttpEntity(headers);
		
		ResponseEntity<Articulo> respuesta = this.restTemplate.exchange(service,HttpMethod.GET,entity,Articulo.class, params);
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
		assertEquals(1, respuesta.getBody().getId());
		assertEquals("Monitor 20", respuesta.getBody().getNombre());
		
	}
	
	@Test
	@Order(4)
	void testFindAll() {
		String service = this.getUrlBase();
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", "application/json");
		
		HttpEntity entity = new HttpEntity(headers);
		
		ResponseEntity<List<Articulo>> respuesta = this.restTemplate.exchange(service, HttpMethod.GET,entity,new ParameterizedTypeReference<List<Articulo>>() {
		});
		
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
		assertEquals(9, respuesta.getBody().size());
	}
	
}
