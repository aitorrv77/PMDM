package application.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import application.model.Cliente;
import net.bytebuddy.agent.VirtualMachine.ForHotSpot.Connection.Response;


@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
public class ClientesControllerTest {

	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	private String getUrlBase() {
		return "http://localhost:"+this.port+"/api-test/clientes";
				
	}
	
	private Integer getCount() {
		String URL = this.getUrlBase()+"/count";
		
		HttpHeaders headers = new HttpHeaders();
		
		headers.set("Accept","application/json");
		
		HttpEntity httpEntity = new HttpEntity(headers);
		
		ResponseEntity<Integer> respuesta = restTemplate.exchange(URL,HttpMethod.GET,httpEntity,Integer.class);
		
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
		
		return respuesta.getBody();
	}
	
	@Test
	@Order(1)
	void testCount() {
		assertEquals(4, this.getCount());
	}
	
	@Test
	@Order(2)
	void testCreate() {
		assertEquals(4, this.getCount());
		
		Cliente cliente = new Cliente();
		cliente.setNombre("Carlos Zaltmann");
		cliente.setDireccion("Plaza las eras");
		
		String service = this.getUrlBase();
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept","application/json");
		
		HttpEntity<Cliente> entity = new HttpEntity<>(cliente,headers);
		
		ResponseEntity<Cliente> respuesta = this.restTemplate.exchange(service,HttpMethod.POST,entity,Cliente.class);
		
		assertEquals(HttpStatus.CREATED, respuesta.getStatusCode());
		
		assertNotNull(respuesta.getBody().getId());
		assertEquals(cliente.getNombre(), respuesta.getBody().getNombre());
		assertEquals(cliente.getDireccion(), respuesta.getBody().getDireccion());
		
		assertEquals(5, this.getCount());
	}
	
	@Test
	@Order(3)
	void testFindById() {
		String service = this.getUrlBase()+"/{id}";
		
		Map<String, String> params = new HashMap<>();
		params.put("id", "1");
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept","application/json");
		
		HttpEntity entity = new HttpEntity(headers);
		
		ResponseEntity<Cliente> respuesta = this.restTemplate.exchange(service, HttpMethod.GET, entity, Cliente.class, params);
		
		assertEquals(HttpStatus.OK,respuesta.getStatusCode());
		assertEquals(1, respuesta.getBody().getId());
		assertEquals("Matt Design", respuesta.getBody().getNombre());
		
	}
	
	@Test
	@Order(4)
	void testFindAll(){
		String service = this.getUrlBase();
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept","application/json");
		
		HttpEntity entity = new HttpEntity(headers);
		
		ResponseEntity<List<Cliente>> respuesta = this.restTemplate.exchange(service, 
																			 HttpMethod.GET,
																			 entity,
																			 new ParameterizedTypeReference<List<Cliente>>(){}
																			 );
		
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
		assertEquals(5,respuesta.getBody().size());
		
	}
	
}
