package application.controller;

import org.junit.jupiter.api.TestMethodOrder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import application.model.Grupo;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
public class GruposControllerTest {
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	private String getUrlBase() {
		return "http://localhost:"+this.port+"/api-test/grupos";
	
	}
	
	private Integer getCount() {
		String URL = this.getUrlBase()+"/count";
		
		HttpHeaders headers = new HttpHeaders();
		
		headers.set("Accept", "application/json");
		
		HttpEntity httpEntity = new HttpEntity(headers);
		
		ResponseEntity<Integer> respuesta = restTemplate.exchange(URL, HttpMethod.GET, httpEntity, Integer.class);
		
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
		
		return respuesta.getBody();
	}
	
	@Test
	@Order(1)
	void testCount() {
		assertEquals(3, this.getCount());
	}
	
	@Test
	@Order(2)
	void testCreate() {
		assertEquals(3,this.getCount());
		
		Grupo grupo = new Grupo();
		grupo.setDescripcion("Hardware");
		
		String service = this.getUrlBase();
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", "application/json");
		
		HttpEntity<Grupo> entity = new HttpEntity<>(grupo, headers);
		
		ResponseEntity<Grupo> respuesta = this.restTemplate.exchange(service,HttpMethod.POST, entity, Grupo.class);
		
		assertEquals(HttpStatus.CREATED,respuesta.getStatusCode());
		
		assertNotNull(respuesta.getBody().getId());
		assertEquals(grupo.getDescripcion(), respuesta.getBody().getDescripcion() );
		
		assertEquals(4, this.getCount());
	}
	
	@Test
	@Order(3)
	void testFindById() {
		String service = this.getUrlBase()+"/{id}";
		
		Map <String, String> params = new HashMap<>();
		params.put("id", "1");
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept","application/json");
		
		HttpEntity entity = new HttpEntity(headers);
		
		ResponseEntity<Grupo> respuesta = this.restTemplate.exchange(service, HttpMethod.GET, entity, Grupo.class, params);
		
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
		assertEquals(1, respuesta.getBody().getId());
		assertEquals("Hardware", respuesta.getBody().getDescripcion());
		}
	
	@Test
	@Order(4)
	void testFindAll() {
		String service = this.getUrlBase();
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", "application/json");
		
		HttpEntity entity = new HttpEntity(headers);
		
		ResponseEntity<List<Grupo>> respuesta = this.restTemplate.exchange(service, HttpMethod.GET,entity, new ParameterizedTypeReference<List<Grupo>>() {});
		
		assertEquals(HttpStatus.OK, respuesta.getStatusCode());
		assertEquals(4, respuesta.getBody().size());
	}
	
	@Test
	@Order(5)
	void testUpdate() {
		
	}
	

}
